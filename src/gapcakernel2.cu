#include <math.h>
#include <stdio.h>
#include <stdint.h>
//@Windows
//#include "f:/work/repos/pgapca/kernel_header.h"
//@Linux
#include "/home/octavian/work/gapca/parallel_gapca/src/kernel_header.h"

static __device__ __inline__ uint32_t __mysmid(){    
  uint32_t smid;    
  asm volatile("mov.u32 %0, %%smid;" : "=r"(smid));    
  return smid;
}

__global__ void euclidean(float *A, float *C)
{
  __shared__ float accumResult[SIZE*TROWS];

  float sA;
  float sB;
  
  // MAPPING
	int bx = blockIdx.x;  // n
	int by = blockIdx.y;
  int bz = blockIdx.z; 
	int ty = threadIdx.y; // 128/256
	int tx = threadIdx.x; // 8 / 4

  int SMid=__mysmid();

  if (TROWS * bx + tx<by * FCOLS + bz) {

  sA = A [(TROWS * bx + tx) * SIZE + ty];
  sB = A [(by * FCOLS + bz) * SIZE + ty];
	__syncthreads();

  accumResult[tx*SIZE+ty] = (sA - sB)*(sA - sB);
	__syncthreads();

	// Parallel tree-reduction
	for (int stride = SIZE/2 ; stride > 0 ; stride >>= 1) {
		if (ty < stride) 
			accumResult[tx*SIZE+ty]	+= accumResult[stride + tx*SIZE+ty];
    __syncthreads();
  }

  if (ty==0)
    if (accumResult[tx*SIZE]>C[SMid*TROWS+tx]) {
      C[SMid*TROWS+tx] = accumResult[tx*SIZE];
      C[14*TROWS+SMid*TROWS+tx] = bx * TROWS + tx;
      C[28*TROWS+SMid*TROWS+tx] = by * FCOLS + bz; 
    }
    
  }

}
