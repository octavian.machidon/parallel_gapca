from numba import autojit, prange, jit
import numpy as np
import h5py 
import sys
import copy
np.set_printoptions(threshold=sys.maxsize)

nthreads=4

@jit(nopython=True, parallel=True, nogil=True)
def parallel_dist(A):
    dist = np.zeros(len(A))
    index = np.zeros(len(A))	
    for i in prange(len(A)-1):
       temp_dist = np.zeros(len(A))
       for j in prange(i+1,len(A)):
       	     #dist[i][j] = np.sqrt(np.sum((A[i]-A[j])**2))
             temp_dist[j] = np.linalg.norm(A[i]-A[j])
      
       dist[i] = np.amax(temp_dist)
       index[i] = np.argmax(temp_dist)
    #print (np.amax(dist), np.argmax(dist), int(index[np.argmax(dist)]))
    return (np.amax(dist), np.argmax(dist), int(index[np.argmax(dist)]))


def euclidean_proj_hyperplane(points, n, middle):
    d = n.dot(np.transpose(middle))
    proj = np.zeros((len(points), points.shape[1]))
    s = 0
    for i in prange(len(points)):
       s = n.dot(np.transpose(points[i]))
       proj[i] = points[i] + (d-s) * n/np.linalg.norm(n)**2
    return (proj)

def pca_estimation(points_t, N):
   v = np.zeros((N, points_t.shape[1]))
   vmax, imax, jmax = parallel_dist(points_t)
   pointsx = points_t[imax]
   pointsy = points_t[jmax]
   v[0] = pointsx-pointsy
   middle = (pointsx+pointsy)/2
   for i in prange(1,N):
      #print(points_t[0][0])
      projections  = euclidean_proj_hyperplane(points_t,v[i-1],middle)
      vmax, imax, jmax = parallel_dist(projections)
      pointsx = projections[imax]
      pointsy = projections[jmax]
      v[i] = pointsx-pointsy
      points_t = copy.deepcopy(projections)
   return (v)

import h5py

if len(sys.argv) != 3:
    sys.stderr.write("usage: python3 gapca_new.py <file dimension> <no. of runs>\ne.g. python3 gapca_new.py 80 10 will read test80x80.h5 and make 10 runs\n".format(sys.argv[0]))
    exit(-1) # or deal with this case in another way
filedim=sys.argv[1]
no_runs=int(sys.argv[2])
in_filename="../data/test"+filedim+"x"+filedim+".h5"
out_filename="results_cpu_ok_"+filedim+"x"+filedim+".txt"

print('Reading ',in_filename)
with h5py.File(in_filename, 'r') as f:
    points : np.int32 = f['/Pavia'][()]

points_tr = np.transpose(points)
N = 6

import time
vsum=0
for i in range(no_runs):
   start = time.time()
   pc =  pca_estimation(points_tr,N)
   end = time.time()
   vtemp=end-start
   vsum=vsum+vtemp
   print('Run ',i,' time ',vtemp)
vsum=vsum/no_runs
print('Mean time ',vsum)

Itransformed = points_tr.dot(np.transpose(pc))
print('writing ',out_filename)
np.savetxt(out_filename, Itransformed)
