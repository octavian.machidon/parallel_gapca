#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include "dsize.h"

static __device__ __inline__ uint32_t __mysmid()
{    
  uint32_t smid;    
  asm volatile("mov.u32 %0, %%smid;" : "=r"(smid));    
  return smid;
}

__global__ void euclidean(short *A, long *C, int FCOLS)
{
    __shared__ long accumResult[SIZE];
    short sA;
    short sB;
  
    // MAPPING
	int bx = blockIdx.x;  // n
	int by = blockIdx.y;
    int bz = blockIdx.z; 
	int ty = threadIdx.y; // 128
	//int tx = threadIdx.x; // 1
    int SMid=__mysmid();

    if (bx<by*FCOLS+bz)
    {
        sA = A [bx * SIZE + ty];
        sB = A [(by * FCOLS + bz) * SIZE + ty];
        __syncthreads();

        accumResult[ty] = (sA - sB)*(sA - sB);
        __syncthreads();


        // Parallel tree-reduction
        for (int stride = SIZE/2 ; stride > 0 ; stride >>= 1) 
        {
            if (ty < stride) 
                accumResult[ty]	+= accumResult[stride + ty];
            __syncthreads();
        }

        // Writing results to output matrix
        if (ty == 0)
            if (accumResult[0]>C[SMid])
            {
                C[SMid] = accumResult[0];
                C[14+SMid] = bx;
                C[28+SMid] = by * FCOLS + bz;
            } 
    } 
}


 void parallelDist(short *A, int FROWS, int FCOLS, int& IMAX, int& JMAX)
 {
   //printf("Entering wrapperfunction...\n");
     // Kernel invocation with one block of N * N * 1 threads
     dim3 gridDim(FROWS*FCOLS,FROWS,FCOLS);
    // printf("Grid size: %d %d %d\n", FROWS*FCOLS,FROWS,FCOLS);
     
     dim3 threadsPerBlock(1, SIZE);
  //   printf("Block size: 1 %d\n", SIZE);

 //    printf("FCOLS = %d\n",FCOLS);

    long *C;
    cudaMallocManaged(&C, 42*sizeof(float));
    for (int i=0;i<3;i++)
        for (int j=0;j<14;j++)
           C[i*14+j]=0;
   

    short *d_values;
    //cudaMalloc(&d_values, FROWS*FCOLS*103*sizeof(short));
    //cudaMemcpy(d_values, A, FROWS*FCOLS*103*sizeof(short), cudaMemcpyHostToDevice);
    cudaMalloc(&d_values, FROWS*FCOLS*SIZE*sizeof(short));
    cudaMemcpy(d_values, A, FROWS*FCOLS*SIZE*sizeof(short), cudaMemcpyHostToDevice);
    
    //printf("Calling CUDA kernel...\n");
    // your code for initialization, copying data to device memory,
    euclidean<<<gridDim, threadsPerBlock>>>(d_values,C,FCOLS); //kernel call
    //your code for copying back the result to host memory & return

    // Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();

    long vmax=0; int pmax=0;
    for (int i=0;i<14;i++)
        if (vmax<C[i]) {
            vmax = C[i];
            pmax = i;
        }
    IMAX = C[14+pmax];
    JMAX = C[28+pmax];
    //printf("%d %d \n",IMAX, JMAX);
 }