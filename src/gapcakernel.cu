#include <math.h>
#include <stdio.h>
#include <stdint.h>
//@Windows
//#include "f:/work/repos/pgapca/kernel_header.h"
//@Linux
#include "/home/octavian/work/gapca/parallel_gapca/src/kernel_header.h"

static __device__ __inline__ uint32_t __mysmid(){    
  uint32_t smid;    
  asm volatile("mov.u32 %0, %%smid;" : "=r"(smid));    
  return smid;
}

__global__ void euclidean(float *A, float *C)
{
	__shared__ float accumResult[SIZE];
  float sA;
  float sB;
  
  // MAPPING
	int bx = blockIdx.x;  // n
	int by = blockIdx.y;
  int bz = blockIdx.z; 
	int ty = threadIdx.y; // 128
	//int tx = threadIdx.x; // 1

  int SMid=__mysmid();

 if (bx<by*FCOLS+bz) {

    sA = A [bx * SIZE + ty];
    sB = A [(by * FCOLS + bz) * SIZE + ty];
    __syncthreads();

    accumResult[ty] = (sA - sB)*(sA - sB);
    __syncthreads();

    // Parallel tree-reduction
    for (int stride = SIZE/2 ; stride > 0 ; stride >>= 1) {
      if (ty < stride) 
        accumResult[ty]	+= accumResult[stride + ty];
      __syncthreads();
    }

    // Writing results to output matrix
    if (ty == 0)
      if (accumResult[0]>C[SMid])
        {
          //if (bx==43) printf("%f %f\n",sqrt(accumResult[ty]),sqrt(C[0]));
          //printf("%d SM id=%d\n",bx,__mysmid());
          C[SMid] = accumResult[0];
          C[14+SMid] = bx;
          C[28+SMid] = by * FCOLS + bz;
        } 
  }
}
