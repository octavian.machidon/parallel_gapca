import numpy 
from operator import truediv 


def compare_results(f1_name, f2_name):
#compares the data in file f1_name and f2_name and returns the absolute error and  the relative error
    T1=[]
    f1 = open(f1_name, "r")
    for line in f1.readlines():
            f1_list = [float(i) for i in line.split(" ") if i.strip()]
            T1 += f1_list
    #print(T1)
    f1.close()

    T2=[]
    f2 = open(f2_name, "r")
    for line in f2.readlines():
            f2_list = [float(i) for i in line.split(" ") if i.strip()]
            T2 += f2_list
    #print(T2)
    f2.close()

    T = numpy.absolute(T1) - numpy.absolute(T2);

    old_err_state = numpy.seterr(divide='raise')
    ignored_states = numpy.seterr(**old_err_state)
    rel = [i / j if j else 0 for i, j in zip(T, numpy.absolute(T1))] 

    #print('%Max absoulte error \n',max(numpy.absolute(T)))
    #print('%Max relative error \n',max(numpy.absolute(rel))*100)
    return (max(numpy.absolute(T)),max(numpy.absolute(rel))*100)