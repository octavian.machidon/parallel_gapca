#include "H5Cpp.h"
#include <vector>
#include <iostream>
#include <string>
#include <stdio.h>
#include <math.h>
#include <memory>
#include <cstdlib>
#include <stdalign.h>
#include <algorithm>
#include <string.h>
#include <omp.h>  //for parallel loops
#include <chrono>  // for high_resolution_clock
#include "dsize.h"

using namespace std;

int N, M, P;
int ROWS, COLS;
int NR_RUNS;

extern void parallelDist(short *A, int FROWS, int FCOLS, int& IMAX, int& JMAX);

void euclidean_proj_hyperplane(short *points, int size1,int size2, int current_run, float* middle, float **v)
{
	int i = 0 ;
	float d  = {0};
	for (i=0;i<size2;i++)
	{
		d += v[current_run][i]*middle[i];   	
	}
	
	double norm = 0;
	
	for (i=0; i<size2; i++)
		norm += v[current_run][i]*v[current_run][i];
	
	norm = sqrt(norm);
	norm = pow(norm,2);
	
	double s = 0;
    #pragma omp parallel num_threads(12)
    {
        #pragma omp for
        for (i=0; i<size1; i++)
        {
            s =0;
            for (int j =0; j<size2; j++)
            {	
                s += v[current_run][j]*points[i*SIZE+j];
            }		
            for (int k =0; k<size2; k++)
            {	
                points[i*SIZE+k] = points[i*SIZE+k] + (d-s)* v[current_run][k]/norm;
            }
        }
	}
}

void pcaEstimation(short *trans_data, int n, int m, float **v)
{
	 int index1 = 0, index2 = 0;
	 long long d = 0;

	 parallelDist(trans_data, N, M, index1, index2);
	 
	 short i_extreme[m] = {0};
	 short j_extreme[m] = {0};
	 float middle[m] = {0};
		 
	 for (int i=0;i<m;i++)
	 {
		i_extreme[i] = trans_data[index1*SIZE+i];
		j_extreme[i] = trans_data[index2*SIZE+i];
		
		v[0][i] = i_extreme[i] - j_extreme[i];
		middle[i] = (float)((i_extreme[i] + j_extreme[i])/2.0);
	 }	 
	
	for (int k=1;k<NR_RUNS;k++)
	{		
		euclidean_proj_hyperplane(trans_data, n, m, k-1, middle, v);
		parallelDist(trans_data, N, M, index1, index2);
		for (int i=0;i<m;i++)
		{
			i_extreme[i] = trans_data[index1*SIZE+i];
			j_extreme[i] = trans_data[index2*SIZE+i];
			
			v[k][i] = i_extreme[i] - j_extreme[i];
		}	
	}

}

int readH5File(string filename,string datasetname, short *trans_data)
{
	// open file
	H5::H5File fid = H5::H5File(filename, H5F_ACC_RDONLY);

	// open dataset, get data-type
	H5::DataSet   dataset    = fid.openDataSet(datasetname);
	H5::DataSpace dataspace  = dataset.getSpace();
	H5::StrType   datatype   = dataset.getStrType();

    H5T_class_t type_class = dataset.getTypeClass();
      /*
       * Get class of datatype and print message if it's an integer.
       */
      if( type_class == H5T_INTEGER )
      {
        cout << "Data set has INTEGER type" <<endl;
      } 
	  else if ( type_class == H5T_FLOAT )
               cout<<"Data set has FLOAT type"<<endl;

    H5::FloatType floattype = dataset.getFloatType();

    H5std_string order_string;
    H5T_order_t order = floattype.getOrder( order_string );
    //cout << order_string << endl;
    size_t size = floattype.getSize();
    cout << "Data size is " << size << endl;
    
    int rank = dataspace.getSimpleExtentNdims();

    hsize_t dims_out[2];
    int ndims = dataspace.getSimpleExtentDims( dims_out, NULL);
    cout << "rank " << rank << ", dimensions " <<
          (unsigned long)(dims_out[0]) << " x " <<
          (unsigned long)(dims_out[1]) << endl;
	
	float *data_out = new float[dims_out[0]*dims_out[1]];

    H5::DataSpace mspace1(rank, dims_out);
	
    //float data_out[dims_out[0]][dims_out[1]];  // buffer for dataset to be read
    dataset.read( data_out, H5::PredType::NATIVE_FLOAT, mspace1, dataspace );

	printf("Writing H5 data to C++ array...\n");
	for (int i = 0; i < dims_out[1]; i++)
    {
        for (int j = 0; j < dims_out[0]; j++)
			trans_data[i*SIZE+j] = static_cast<short>(data_out[j*dims_out[1]+ i]);
		for (int j = dims_out[0]; j < SIZE; j++)
			trans_data[i*SIZE+j] = 0;
    }
	return 0;
}


int main(int argc, char* argv[])
{
	// ./gapca.so paviaU 20 20 103 6
	if ( argc > 1 )
	{
		N = atoi(argv[2]);
		M = atoi(argv[3]);
		P = atoi(argv[4]);
		NR_RUNS = atoi(argv[5]);
		ROWS = N * M + 5;
		COLS = P + 5;
	}
	else {
		printf("Usage: ./object.so <dataset> <dim1> <dim2> <dim3> <ncomp>\n");
		exit(0);
	}
	
	char * fname = new char[strlen(argv[1])+strlen(argv[2])+strlen(argv[3])+strlen(argv[4])+15];
	const char* datafolder = "../data/";
	strcpy(fname,datafolder);
	strcat(fname,argv[1]);
    strcat(fname,"_");
	strcat(fname,argv[2]);
	strcat(fname,"x");
	strcat(fname,argv[3]);
	strcat(fname,"x");
	strcat(fname,argv[4]);
	strcat(fname,".h5");
	printf("Reading file ");
	for(int i = 0 ; i < strlen(fname) ; i ++ ){
      printf("%c",fname[i]);
	}
	printf("...\n");
	printf("Number of Components: %d \n", NR_RUNS);
    int i = 0,index1 = 0,index2 = 0; 
	long long d = 0;
  	short *arr = (short *)malloc(ROWS * SIZE * sizeof(short)); 
 	
	readH5File(fname, "data", arr);
  
	float **v = (float **)malloc(NR_RUNS * sizeof(float *));
	for (int i=0; i<NR_RUNS; i++)  
		v[i] = static_cast<float*>(malloc(P* sizeof(float *)));

	
	// Record start time
	auto start = std::chrono::high_resolution_clock::now();
	// Portion of code to be timed
	pcaEstimation(arr,N*M,P,v);
	// Record end time
	auto finish = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time AVX: " << elapsed.count() << " s\n";
	
	free(arr);

}