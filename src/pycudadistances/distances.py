#from utils.py
from scipy import sparse
import numpy as np
#@Catalin
import pycuda.driver as cuda       # PyCuda In, Out helpers



def _assert_all_finite(X):
    """Like assert_all_finite, but only for ndarray."""
    if X.dtype.char in np.typecodes['AllFloat'] and not np.isfinite(X.sum()) \
      and not np.isfinite(X).all():
            raise ValueError("Array contains NaN or infinity.")


def assert_all_finite(X):
    """Throw a ValueError if X contains NaN or infinity.

    Input MUST be an np.ndarray instance or a scipy.sparse matrix."""

    # First try an O(n) time, O(1) space solution for the common case that
    # there everything is finite; fall back to O(n) space np.isfinite to
    # prevent false positives from overflow in sum method.
    _assert_all_finite(X.data if sparse.issparse(X) else X)


def safe_asarray(X, dtype=None, order=None):
    """Convert X to an array or sparse matrix.

    Prevents copying X when possible; sparse matrices are passed through."""
    if sparse.issparse(X):
        assert_all_finite(X.data)
    else:
        X = np.asarray(X, dtype, order)
        assert_all_finite(X)
    return X



#-*- coding:utf-8 -*-

"""Utilities to evaluate pairwise distances or metrics between 2
sets of points.

"""

# Authors: Vinnicyus Gracindo <vini.gracindo@gmail.com>
# License: GNU GPL.

import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import pycuda.autoinit
from pycuda import gpuarray


from math import sqrt
import numpy
#from utils import safe_asarray
#from sklearn.utils.validation import safe_asarray

#Only main Device
MAX_THREADS_PER_BLOCK = 1024
#print(MAX_THREADS_PER_BLOCK)

#BLOCK_SIZE = int(sqrt(MAX_THREADS_PER_BLOCK))
BLOCK_SIZE=32

def euclidean_distances(Xsize, fdim_rows, fdim_cols, bsize, nbands, X):
    Xpad = np.zeros((Xsize, bsize), dtype=numpy.float32)
    Xpad[:Xsize,:nbands] = X
    Xpad = Xpad.tolist()
    X = Xpad
    
    X = safe_asarray(X, numpy.float32)

    bdim = (1, bsize, 1)
    gdim = (Xsize,int(fdim_rows),int(fdim_cols))
        
    #@Linux
    with open('../src/gapcakernel.cu', 'r') as f:
    
    #@Windows
    #with open('src/gapcakernel.cu', 'r') as f:
            cudaCode = f.read()
    mod = SourceModule(cudaCode)
    func = mod.get_function("euclidean")
   
    X_gpu = gpuarray.to_gpu(X)
    gpu_solution = gpuarray.empty((3, 14), np.float32)
    
    #Modified by @Catalin
    func(X_gpu, gpu_solution, grid=gdim, block=bdim)
    #func(cuda.In(X_gpu), cuda.Out(gpu_solution), grid=gdim, block=bdim)

    solution = gpu_solution.get()
    posmax=np.argmax(solution[:1])
    #print(sqrt(solution[0][posmax]), int(solution[1][posmax]), int(solution[2][posmax]))
    return (sqrt(solution[0][posmax]), int(solution[1][posmax]), int(solution[2][posmax]))
