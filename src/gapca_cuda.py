from numba import autojit, prange, jit
from pycudadistances.distances import euclidean_distances
#from pycudadistances.distances2 import euclidean_distances
import numpy as np
import h5py 
import sys
import copy

def euclidean_proj_hyperplane(points, n, middle):
    d = n.dot(np.transpose(middle))
    proj = np.zeros((len(points), points.shape[1]))
    s = 0
    for i in prange(len(points)):
       s = n.dot(np.transpose(points[i]))
       proj[i] = points[i] + (d-s) * n/np.linalg.norm(n)**2
    return (proj)

def pca_estimation(points_t, N, fdim_rows, fdim_cols, bsize, fdim_bands):
   v = np.zeros((N, points_t.shape[1]))
   vmax, imax, jmax = euclidean_distances(points_t.shape[0], fdim_rows, fdim_cols, bsize, fdim_bands, points_t.tolist())
   pointsx = points_t[imax]
   pointsy = points_t[jmax]
   v[0] = pointsx-pointsy
   middle = (pointsx+pointsy)/2
   for i in prange(1,N):
      projections  = euclidean_proj_hyperplane(points_t,v[i-1],middle)
      vmax, imax, jmax = euclidean_distances(projections.shape[0], fdim_rows, fdim_cols, bsize, fdim_bands, projections.tolist())
      pointsx = projections[imax]
      pointsy = projections[jmax]
      v[i] = pointsx-pointsy
      points_t = copy.deepcopy(projections)
   return (v)

def nextPowerOf2(n): 
    p = 1
    if (n and not(n & (n - 1))): 
        return n 
    while (p < n) : 
        p <<= 1   
    return p; 

import h5py
if len(sys.argv) != 7:
    sys.stderr.write("usage: python3 gapca_cuda.py <dataset> <file dimension> <no. of components> <no. of runs>\ne.g. python3 gapca_new.py paviaU 20 20 103 6 2 will read paviaU_20x20x103.h5 and make 2 runs computing 2 PCs\n".format(sys.argv[0]))
    exit(-1) # or deal with this case in another way
dataset=sys.argv[1]
filedim_rows=sys.argv[2]
filedim_cols=sys.argv[3]
filedim_bands=sys.argv[4]
no_comp=int(sys.argv[5])
no_runs=int(sys.argv[6])

#@Linux version
in_filename="../data/"+dataset+"_"+filedim_rows+"x"+filedim_cols+"x"+filedim_bands+".h5"

#@Windows version
#in_filename="data/test"+filedim_rows+"x"+filedim_cols+".h5"

out_filename="results_cuda_"+dataset+"_"+filedim_rows+"x"+filedim_cols+"x"+filedim_bands+"_"+str(no_comp)+"PC"+".txt"

print('Reading ',in_filename)
with h5py.File(in_filename, 'r') as f:
    points : np.int32 = f['/data'][()]
points_tr = np.transpose(points)
nrows=points_tr.shape[0]

#Modified by @Catalin
ncols=nrows
#ncols = points_tr.shape[1]

blocksize = nextPowerOf2(int(filedim_bands))

ndim=points_tr.shape[1]
#@Catalin
#print(nrows,ncols,ndim)
file_header=open("kernel_header.h","w")
file_header.write("#define NROWS "+str(nrows)+"\n")
file_header.write("#define NCOLS "+str(ncols)+"\n")
file_header.write("#define NDIM "+str(ndim)+"\n")
file_header.write("#define FROWS "+str(filedim_rows)+"\n")
file_header.write("#define FCOLS "+str(filedim_cols)+"\n")
file_header.write("#define FBANDS "+str(filedim_bands)+"\n")
file_header.write("#define SIZE "+str(blocksize)+"\n")
file_header.write("#define TROWS "+str(int(1024/blocksize))+"\n")
file_header.close()

import time
vsum=0
for i in range(no_runs):
   start = time.time()
   pc =  pca_estimation(points_tr,no_comp,filedim_rows,filedim_cols,blocksize,int(filedim_bands))
   end = time.time()
   vtemp=end-start
   vsum=vsum+vtemp
   print('Run ',i,' time ',vtemp)
vsum=vsum/no_runs
print('Mean time ',vsum)

Itransformed = points_tr.dot(np.transpose(pc))
print('writing ',out_filename)
np.savetxt(out_filename, Itransformed)


  

