#/bin/bash

#-------------------------------------------------------------
#   The Crop parameters
TEST_DATASET=paviaU
TEST_LIST_N=(20 40 80 100 200 250 300 400 610)
TEST_LIST_M=(20 40 80 100 200 250 300 340 340)
TEST_NBANDS=103
TEST_NCOMPONENTS=(1 3 5)
TEST_MIN=20
TEST_MAX=200
TEST_RUNS=2

TEST_TYPE=GPU


#-------------------------------------------------------------
#   Paths
PROJ_DIR=`pwd`
INPUT_FILE_BASE="data/paviaU"
INPUT_FILE_EXT="h5"

LOG_PATH=$PROJ_DIR/../logs

DATE=`date +day%j_hour%H_min%M`
DATE=PGAPCA_${DATE}

LOG=${LOG_PATH}/${TEST_TYPE}_${DATE}


#-------------------------------------------------------------
#   Python

PYTHON="python3"
BENCH="gapca_cuda.py"

#-------------------------------------------------------------
#   Computed parameters
#suffix_results= "${TEST_TYPE}_N${TEST_MIN}-${TEST_MAX}"

echo -e "The project dir is:" ${PROJ_DIR}
echo -e "The log dir is:" ${LOG_PATH}
echo -e "The log file is:" ${LOG}

mkdir $LOG_PATH
touch $LOG

#mkdir ${LOG_PATH}/logs/${DATE}

for ((index_test=0;index_test<${#TEST_LIST_N[@]};index_test++)); do						
    test_i=`echo ${TEST_LIST_N[$index_test]}`
    test_j=`echo ${TEST_LIST_M[$index_test]}`
    test_q=`echo ${TEST_NBANDS}`
    #echo -e "index_test=${index_test}"
    for ((index_ncomp=0;index_ncomp<${#TEST_NCOMPONENTS[@]};index_ncomp++)); do
      test_ncomps=`echo ${TEST_NCOMPONENTS[$index_ncomp]}`
      echo "Running test ${test_i}x${test_j}x${test_q} for $test_ncomps principal components..."
      time $PYTHON $BENCH $TEST_DATASET $test_i $test_j $test_q $test_ncomps $TEST_RUNS &>>${LOG}
      echo -e >>${LOG}
      echo -e "Done test ${test_i}x${test_j}x${test_q} for $test_ncomps principal components!\n"
    done
done